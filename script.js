
/* 
    alert funkcija Ismeta vartotojui pranesima i ekrana
*/
//alert("labas rytas");

var skaicius1 = 3;
var skaicius2 = "3";
var rezultatas;
var vardas = "Eimantas";

skaicius1++; // skaicius1 = skaicius1 + 1;
skaicius1--; // skaicius = skaicius - 1;


rezultatas = skaicius1 + skaicius2 * 5;

alert(rezultatas);

var x = "Jonas";
var y = "Petras";

x += " ";
x += y;

// x += " " + y;

alert(x);

var a = 2;
a++;

/* a++; b = a * 2; */
var b = ++a * 2; 

/* c = a / 2; a--; */
var c = a-- / 2; 









// Galime sujungti dvi tekstines eilutes
vardas += " Kasperiunas";

alert(vardas);


var a = 1;

a += 2 + 3; // tas pats kas uzrasytume a = a + 2 + 3

a = 2 + 3; // 5

a -= 3; // a = a - 3;

a *= 2; // a = a * 2;

// a = 4

var a = 1;
a += 2 + 3;
a -= 4;
a *= a + 3; 






rezultatas = skaicius1 / skaicius2; // 1 / 5

skaicius1 = 10;

rezultatas = skaicius1 - skaicius2; // 10 - 5

console.log("skaicius3 yra lygus: " + rezultatas);
// console.log(rezultatas);


// alert(skaicius1 + skaicius2);


/* 
    console.log funkcija spausdina kintamuosius/teksta ir tt 
    i console tab'a musu inspektoriuje
*/
console.log("labas konsole");
console.log(skaicius1);
console.log(skaicius1 * skaicius2);

/* 
    document.write funkcija spaudina kintamuosius/teksta 
    i musu body elemento dalies pabaiga. Galime spausdinti ir html elementus
*/
document.write("<h1>Headingas</h1>");
document.write("Labas, ");
document.write(vardas);