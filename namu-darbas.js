var plotis = 10;
var aukstis = 5;

var perimetras = plotis + aukstis;
perimetras *= 2;

var plotas = plotis * aukstis;

var istrizaine = plotis * plotis + aukstis * aukstis;
istrizaine = Math.sqrt(istrizaine); // saknies traukimas
// istrizaine = istrizaine.toFixed(2);

// document.write("<div>Staciakampio perimetras: " + perimetras +  "</div>");

console.log("Staciakampio perimetras: " + perimetras);

console.log("Staciakampio plotas: " + plotas);

console.log("Staciakampio istrizaine: " + istrizaine.toFixed(2));


document.getElementById("perimetras").innerHTML = "Staciakampio perimetras: <strong>" + perimetras + "</strong>";

var plotasDiv = document.getElementById("plotas");
plotasDiv.innerText = `Staciakampio plotas: ${plotas} `;

var istrizaineSpan = document.getElementById("istrizaine");
istrizaineSpan.innerText = istrizaine.toFixed(2);

/* 

Elementu pasirinkimas pagal klase
var istrizainesClass = document.getElementsByClassName('istrizaine');
console.log(istrizainesClass);

istrizainesClass[0].innerText = "labas";

Elementu pasirinkimas pagal css selektoriu
document.querySelectorAll(".menu li");
*/

