
var lentele = document.getElementById('daugybos-lentele');

for(var i = 1; i <= 10; i++) {
    // sukuriu lenteles eilute
    var row = document.createElement("tr");


    // sukuriu eilutes stulpelius
    for(var j = 1; j <= 10; j++) {
        var col = document.createElement("td");
        
        if(i === j) {
            col.innerHTML = "<strong>" + i * j + "</strong>";
        } else {
            col.innerHTML = i * j;
        }
        

        row.appendChild(col);
    }

    lentele.appendChild(row);
}