
var masinos = [
    ["2000", "LRS123", 5000, 118],
    ["2001", "LRS321", 6000, 28],
    ["2008", "AAA999", 200, 568]
];

var greitis = 0;


// greicio skaiciavimas
for(var i = 0; i < masinos.length; i++ ) {
    greitis = (masinos[i][2] / masinos[i][3]) * 3600 / 1000;
    // Prie masinos duomenu pridedame suskaiciuota greiti km/h
    masinos[i].push(greitis);
}




// duomenu atvaizdavimas
var sarasas = document.getElementById("duomenys");

for(var i = 0; i < masinos.length; i++) {
    sarasas.innerHTML += "<li>" + masinos[i][1] + " " + masinos[i][4].toFixed(2) + " km/h</li>";
}

// vidurkio skaiciavimas 
var suma = 0;
for(var i = 0; i < masinos.length; i++) {

    // susumuojam visu greiciu suma
    suma += masinos[i][4];
}

var vidurkis = suma / masinos.length;

sarasas.innerHTML += "<li>Vidutinis greitis visu masinu: " + vidurkis.toFixed(2) + " km/h</li>";




var lentele = document.getElementById("duomenu-lentele");



for(var i = 0; i < masinos.length; i++) {

    // Sukuriame tr elementa
    var row = document.createElement("tr");
    // Sukurti stulpeli kiekvienam masinos informacijos elementui
    for(var j = 0; j < masinos[i].length; j++){
        var col = document.createElement("td");
        col.innerText = masinos[i][j];

        // Pridedame sukurta stulpeli i savo eilute
        row.appendChild(col);
    }

    // Sukurta eilute prideda i lentele atvaizdavimui
    lentele.appendChild(row);

}

// pabaiga





