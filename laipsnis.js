// skaiciaus kelimas laipsniu
function keltiLaipsniu(skaicius, laipsnis) {
    var rezultatas = skaicius;
    for(var i = 0; i < laipsnis; i++) {
        rezultatas = rezultatas * skaicius;
     }

    console.log(rezultatas);
     
    return rezultatas;
}

// 
var skaiciuotiButton = document.getElementById("skaiciuoti");
var skaiciusInput = document.getElementById("skaicius");
var laipsnisInput = document.getElementById("laipsnis");
var rezultatasDiv = document.getElementById("rezultatas");

skaiciusInput.addEventListener('keyup', function() {
    rezultatasDiv.innerHTML = keltiLaipsniu(skaiciusInput.value, laipsnisInput.value);
}); 

laipsnisInput.addEventListener('keyup', function() {
    rezultatasDiv.innerHTML = keltiLaipsniu(skaiciusInput.value, laipsnisInput.value);
}); 

skaiciuotiButton.addEventListener("mouseover", function() {
    

    skaiciusInput.value = "";
    laipsnisInput.value = "";

});