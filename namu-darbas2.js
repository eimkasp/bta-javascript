

var trikampis = [3, 4, 5];
var lygiakrastis = false;
var lygiasonis = false;
var statusis = false;
var ivairakrastis = false;

var plotas;

var perimetras;

var arTrikampis = true;


if (arTrikampis) {
    // triktiname ar trikampis yra lygiakrastis
    if (trikampis[0] === trikampis[1] && trikampis[1] === trikampis[2]) {
        console.log("lygiakrastis");
        lygiakrastis = true;
    }
    // tikriname ar trikampis yra lygiasonis
    else if (trikampis[0] === trikampis[1] || trikampis[0] === trikampis[2] || trikampis[1] === trikampis[2]) {
        console.log("lygiasonis");
        lygiasonis = true;
    }
    // Triktiname ar trikampis yra statusis
    else if (Math.pow(trikampis[0], 2) + Math.pow(trikampis[1], 2) === Math.pow(trikampis[2], 2)) {
        console.log("statusis");
        statusis = true;
    } 
    
    // Tikriname ar trikampis yra ivairakrastis
    if(trikampis[0] !== trikampis[1] && trikampis[0] !== trikampis[2] && trikampis[1] !== trikampis[2]) {
        console.log("ivairiakrastis");
        ivairakrastis = true;
    }
    perimetras = trikampis[0] + trikampis[1] + trikampis[2];
    var pusperimetris = perimetras / 2;
    plotas = pusperimetris * (pusperimetris - trikampis[0]) * (pusperimetris - trikampis[1]) * (pusperimetris - trikampis[2]);
    plotas = Math.sqrt(plotas);

    console.log("Trikampio plotas yra: " + plotas.toFixed(2));
} else {
    console.log("Cia ne trikampis");
}

